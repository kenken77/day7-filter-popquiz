(function() {

    "use strict";
    angular.module("PopQuizApp").controller("controllerName", controllerName);

    controllerName.$inject = ['$scope', '$filter'];

    function controllerName($scope, $filter) {
        var self = this; //vm
        self.search = "";
        self.propertyName = 'id';
        self.reverse = false;

        self.items = [
            { id: 1, name: 'ABC' },
            { id: 4, name: 'Jkl' },
            { id: 3, name: 'GHI' },
            { id: 5, name: 'ABCD' },
            { id: 2, name: 'DEF' }
        ];

        self.items2 = self.items;

        /*
        $scope.$watch('ctrl.search', function(val) {
            self.items = $filter('filter')(self.items2, val);
        });*/

        self.sortBy = function(propertyName) {
            console.log("Pass in ..." + propertyName);
            console.log("Ctrl ... " + self.propertyName);
            if (self.propertyName === propertyName) {
                console.log(self.reverse);
                console.log(!self.reverse);
            } else {
                console.log(self.reverse);
                self.reverse = false;
                console.log(self.reverse);
            }
            self.reverse = (self.propertyName === propertyName) ? !self.reverse : false;
            self.propertyName = propertyName;
        };
    }

})();