/**
 * Client side code.
 */
(function() {
    "use strict";
    angular.module("PopQuizApp").controller("PopQuizCtrl", PopQuizCtrl);

    PopQuizCtrl.$inject = ["$http"];

    function PopQuizCtrl($http) {
        var self = this; // vm

        self.qnNum = 1;
        self.numQuestions = 0;

        self.quiz = {

        };

        self.finalanswer = {
            id: 0,
            value: "",
            comments: "",
            message: ""
        };

        self.initForm = function() {
            $http.get("/numQuestions")
                .then(function(result) {
                    console.log(result);
                    self.numQuestions = result.data;
                }).catch(function(e) {
                    console.log(e);
                });

            $http.get("/popquizes")
                .then(function(result) {
                    console.log(result);
                    self.quiz = result.data;
                }).catch(function(e) {
                    console.log(e);
                });
        };

        self.initForm();


        self.submitDisabled = false;
        self.showResult = false;

        self.submitQuiz = function() {
            console.log("submitQuiz !!!");
            self.finalanswer.id = self.quiz.id;
            $http.post("/submit-quiz", self.finalanswer)
                .then(function(result) {
                    console.log(result);
                    if (result.data.isCorrect) {
                        self.finalanswer.message = "It's CORRECT !";
                    } else {
                        self.finalanswer.message = "WRONG !";
                    }
                    if (result.data.endQuiz) {
                        console.log("End of quiz, score = " + result.data.score);
                        self.endQuiz = true;
                        self.score = result.data.score;
                    }
                }).catch(function(e) {
                    console.log(e);
                });
            self.submitDisabled = true;
        };

        self.getNext = function() {
            self.finalanswer.message = "";
            self.finalanswer.value = "";
            self.submitDisabled = false;
            $http.get("/popquizes")
                .then(function(result) {
                    console.log(result);
                    self.quiz = result.data;
                    self.qnNum++;
                }).catch(function(e) {
                    console.log(e);
                });
        };

        self.EndQuiz = function() {
            self.showResult = true;
        }

    }

})();